<?php
namespace Deployer;

require 'recipe/common.php';
require 'vendor/deployer/recipes/recipe/slack.php';

// Project name
set('application', 'Common_citizen_store');

// Project repository
set('repository', 'git@bitbucket.org:cddevteam/commoncitizenstore-child.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('slack_webhook', 'https://hooks.slack.com/services/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');

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);


// Hosts



// Tasks

task('git_pull', function () {
    $result = run('git pull');
    writeln("Git Pull Result: $result");
});

set('git_status', function () {
    return run('git status');
});


set('slack_success_text', 'push Successful, git status afterwards: ```{{git_status}}``` successful');

desc('Deploy your project');
task('deploy', [
//    'deploy:info',
//    'deploy:prepare',
//    'deploy:lock',
//    'deploy:release',
//    'deploy:update_code',
//    'deploy:shared',
//    'deploy:writable',
//    'deploy:vendors',
//    'deploy:clear_paths',
//    'deploy:symlink',
//    'deploy:unlock',
//    'cleanup',
    'git_pull',
    'success'
]);

after('success', 'slack:notify:success');

after('deploy:failed', 'slack:notify:failure');

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
