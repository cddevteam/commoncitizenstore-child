<?php
add_action('wp_enqueue_scripts', 'ark_register_styles');
/**
 * Enqueues styles.
 */
function ark_register_styles()
{
    // Bootstrap
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/css/bootstrap.min.css', [], '3.3.6');


    // Theme javascript plugins
    wp_enqueue_style('jquery.mCustomScrollbar', get_template_directory_uri() . '/assets/plugins/scrollbar/jquery.mCustomScrollbar.css', [], '3.1.12');
    wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/assets/plugins/owl-carousel/assets/owl.carousel.css', [], '1.3.2');
    wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.css', [], '3.5.1');
    wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/assets/plugins/magnific-popup/magnific-popup.css', [], '1.1.0');
    wp_enqueue_style('cubeportfolio', get_template_directory_uri() . '/assets/plugins/cubeportfolio/css/cubeportfolio.min.css', [], '3.8.0');

    // Icon Fonts
    if (class_exists('ffContainer')) {
        ark_wp_enqueue_framework_icon_fonts_styles();
    } else {
        wp_enqueue_style('ark-modified-font-awesome', get_template_directory_uri() . '/no-ff/iconfonts/ff-font-awesome4/ff-font-awesome4.css');
        wp_enqueue_style('ark-modified-font-et-line', get_template_directory_uri() . '/no-ff/iconfonts/ff-font-et-line/ff-font-et-line.css');
    }
    
    // Theme Styles
    wp_enqueue_style('ark-one-page-business', get_template_directory_uri() . '/assets/css/one-page-business.css');
    wp_enqueue_style('ark-landing', get_template_directory_uri() . '/assets/css/landing.css');

    // Stylesheet from the PARENT theme
    wp_enqueue_style('ark-style', get_template_directory_uri().'/style.css');

    // Stylesheet from the CHILD theme
    wp_enqueue_style('ark-style-child', get_stylesheet_directory_uri().'/style.css');
    wp_enqueue_style('ark-style-igi', get_stylesheet_directory_uri().'/style_igi.css');
    wp_enqueue_style('ark-style-child-alps', get_stylesheet_directory_uri().'/style_alps.css');
    wp_enqueue_style('ark-style-child-mark', get_stylesheet_directory_uri().'/style_mark.css');
    wp_enqueue_style('ark-style-child-gui', get_stylesheet_directory_uri().'/style_gui.css');
    wp_enqueue_style('ark-style-child-troi', get_stylesheet_directory_uri().'/style_troi.css');
    wp_enqueue_style('ark-style-child-iris', get_stylesheet_directory_uri().'/style_iris.css');
    wp_enqueue_style('vue-syle', get_stylesheet_directory_uri().'/dist/app.css');
    // Custom fonts
    wp_enqueue_style('ChronicleDisplay-Black', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-Black/styles.css');
    wp_enqueue_style('ChronicleDisplay-Bold', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-Bold/styles.css');
    wp_enqueue_style('ChronicleDisplay-Roman', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-Roman/styles.css');
    wp_enqueue_style('ChronicleDisplay-Semi', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-Semi/styles.css');
    wp_enqueue_style('ChronicleDisplay-Light', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-Light/styles.css');
    wp_enqueue_style('ChronicleDisplay-XLight', get_stylesheet_directory_uri().'/fonts/ChronicleDisplay-XLight/styles.css');

    wp_enqueue_script('vue-script', get_stylesheet_directory_uri() . '/dist/app.js', ['jquery'], null, true);

    wp_enqueue_style('Lato-Bold', get_stylesheet_directory_uri().'/fonts/Lato-Bold/styles.css');
    wp_enqueue_style('Lato-Hairline', get_stylesheet_directory_uri().'/fonts/Lato-Hairline/styles.css');
    wp_enqueue_style('Lato-Light', get_stylesheet_directory_uri().'/fonts/Lato-Light/styles.css');
    wp_enqueue_style('Lato-Regular', get_stylesheet_directory_uri().'/fonts/Lato-Regular/styles.css');




    if (class_exists('ffContainer')) {
        ark_wp_enqueue_theme_google_fonts_styles();
    } else {
        wp_enqueue_style('ark-fonts', ark_fonts_url(), [], null);
        wp_enqueue_style('ark-fonts-family', get_template_directory_uri() . '/no-ff/no-ff-fonts.css');
    }
    if (class_exists('WooCommerce')) {
        wp_enqueue_style('ark-woocommerce', get_template_directory_uri() . '/woocommerce/woocommerce.css');
    }
    $url = ark_get_custom_color_css_url();
    wp_enqueue_style('ark-colors', $url);

    // TwentyTwenty
    wp_enqueue_style('twentytwenty', get_template_directory_uri() . '/assets/plugins/twentytwenty/css/twentytwenty.css', []);
}
add_action('after_setup_theme', 'yourtheme_setup');
function yourtheme_setup()
{
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}

/**
 * SHOW USER ID IN USER TABLE
 *
 */
add_filter('manage_users_columns', 'pippin_add_user_id_column');
function pippin_add_user_id_column($columns)
{
    $columns['user_id'] = 'User ID';
    return $columns;
}
 add_action('manage_users_custom_column', 'pippin_show_user_id_column_content', 10, 3);
function pippin_show_user_id_column_content($value, $column_name, $user_id)
{
    $user = get_userdata($user_id);
    if ('user_id' == $column_name) {
        return $user_id;
    }
    return $value;
}

/**
* GURU CUSTOM SHOW MENUs as SHORTCODES*/
function print_menu_shortcode($atts, $content = null)
{
    extract(shortcode_atts([ 'name' => null, ], $atts));
    return wp_nav_menu([ 'menu' => $name, 'echo' => false ]);
}
add_shortcode('menu', 'print_menu_shortcode');

/**
 * @snippet       Display Advanced Custom Fields @ Single Product - WooCommerce
  */
  
/*add_action( 'woocommerce_product_thumbnails', 'bbloomer_display_acf_field_under_images', 30 );
  function bbloomer_display_acf_field_under_images() {
  echo '<b>Trade Price:</b> ' . get_field('brand');
  // Note: 'trade' is the slug of the ACF
}*/

/**
 * Change layout from left to right image and sale fash*/
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
/*add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 90 );*/

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_show_product_images', 1);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
/*add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );*/

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_single_product_summary', 'WC_Structured_Data::generate_product_data()', 60);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_rating', 40);
/*add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_price', 10 );*/
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_meta', 30);
/*add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_sharing', 50 );*/
add_action('woocommerce_before_single_product_summary', 'WC_Structured_Data::generate_product_data()', 60);

/*Enable Product Slider & Lightbox - Woo.
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
add_theme_support( 'wc-product-gallery-zoom' );
add_filter( 'current_theme_supports-wc-product-gallery-slider', '__return_true' );
add_filter( 'current_theme_supports-wc-product-gallery-lightbox', '__return_true' );
add_filter( 'current_theme_supports-wc-product-gallery-zoom', '__return_true' );


/* CHANGE word None in Meta Filter URLs*/
    function change_none_text_sogja9h48hg()
    {
        return 'Clear Filter';
    }
    add_action('prdctfltr_none_text', 'change_none_text_sogja9h48hg');

add_action('woocomerce_archive_description', 'wc_print_notices', 50);
remove_action('woocommerce_before_shop_loop', 'wc_print_notices', 10);

// Or moving result section below
/*remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );*/
/*add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 10 );*/

/* Show pagination on the top of shop page */
add_action('woocommerce_before_shop_loop', 'woocommerce_pagination', 20);
/* Remove pagination on the bottom of shop page */
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

// Moving the catalog section up
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);


 




/*  Set product columns for specific category */

    add_filter('loop_shop_columns', 'loop_columns');
    if (!function_exists('loop_columns')) {
        function loop_columns()
        {
            return 4;
        }

        /* Set product per page for specific category */
        add_action('woocommerce_product_query', 'cdx_woocommerce_products_per_page', 1, 50);
        function cdx_woocommerce_products_per_page($query)
        {
            if ($query->is_main_query() && isset($query->query['product_cat'])) {
                switch ($query->query['product_cat']) {

                case 'cannabis':
                    $query->set('posts_per_page', '6');
                break;
                case 'lifestyle':
                    $query->set('posts_per_page', '6');
                break;
case 'accessories':
                    $query->set('posts_per_page', '6');
                break;
                default:
                    $query->set('posts_per_page', '6');
            }
            }
        }
    }

add_filter('woocommerce_product_reviews_tab_title', 'add_stars_to_review_tab', 98);
function add_stars_to_review_tab($title)
{
    global $product;

    $average_rating = $product->get_average_rating();
    $rating_count = $product->get_rating_count();
    $review_count = $product->get_review_count();

    if (! empty($average_rating) && $average_rating > 0) {
        return '<div>' . $title . '</div>' . wc_get_rating_html($average_rating) . sprintf(
            '<div class="stars">%s / 5 (%s %s)</div>',
            $average_rating,
            $review_count,
            _n("review", "reviews", $review_count, "woocommerce")
        );
    }
    return $title;
}

function displayProductName($item)
{
    $productName = get_the_title($item['id']);
    return $productName;
}
add_shortcode('product_name', 'displayProductName');


add_filter('woocommerce_dropdown_variation_attribute_options_args', 'bt_dropdown_choice');
//
/**
 * Change the custom dropdown  "Choose an option" text on the front end
 */
function bt_dropdown_choice($args)
{
    if (is_product()) {
        $args['show_option_none'] = "Please Select"; // Change your text here
    }
    return $args;
}

/**
 * Removes the tabs that displays the product attributes.
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab
    return $tabs;
}

/**
 * @snippet       Add "Quantity" Label in front of Add to Cart Button - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21986
 * @author        Rodolfo Melogli
 * @testedwith    WC 3.5.1
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_action('woocommerce_before_add_to_cart_quantity', 'bbloomer_echo_qty_front_add_cart');
 
function bbloomer_echo_qty_front_add_cart()
{
    echo '<div class=".ivpa_title">Qty: </div>';
}

/**
 * Show term URL
 */
add_filter('woocommerce_attribute', 'make_product_atts_linkable', 10, 3);
function make_product_atts_linkable($text, $attribute, $values)
{
    $new_values = [];
    foreach ($values as $value) {
        if ($attribute['is_taxonomy']) {
            $term = get_term_by('name', $value, $attribute['name']);
            $url = get_term_meta($term->term_id, 'attribute_url', true);
            if (! empty($url)) {
                $val = '<a href="' . esc_url($url) . '" title="' . esc_attr($value) . '">' . $value . '</a>';
                array_push($new_values, $val);
            } else {
                array_push($new_values, $value);
            }
        } else {
            $matched = preg_match_all("/\[([^\]]+)\]\(([^)]+)\)/", $value, $matches);
            if ($matched && count($matches) == 3) {
                $val = '<a href="' . esc_url($matches[2][0]) . '" title="' . esc_attr($matches[1][0]) . '">' . sanitize_text_field($matches[1][0]) . '</a>';
                array_push($new_values, $val);
            } else {
                array_push($new_values, $value);
            }
        }
    }
    $text = implode(', ', $new_values);
    return $text;
}


function custom_pre_get_posts_query($q)
{
    if (is_shop() && is_page('shop')) {
        $tax_query = (array) $q->get('tax_query');
        $tax_query[] = [
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => [ 'cannabis' ]
        ];
        $q->set('tax_query', $tax_query);
        $q->set('posts_per_page', '6');
    } elseif (is_page('all-products')) {
        // global $wp_query;
        // $args = [
        //        'post_type' => 'product',
        //        'query' => [
        //         'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
        //        ],
        //        'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
        //        'paginate' => true,
        //        'is_page' => true,
        //        'pages' => true,
        //        'prev_next' => true,
        //        'posts_per_page' => '6'
        //        ];
        // $posts = query_posts($args, $wp_query->query);
        // you've to create the template you want to use here first
    // $hide_empty = true ;
        // $cat_args = array(
        //     'hide_empty' => $hide_empty,
        // );
        // $product_categories = get_terms( 'product_cat', $cat_args );
        // $terms = wp_list_pluck( $product_categories, 'slug' );
        // $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        // $query_args['paged']= $paged;
        // $query_args['posts_per_page']= '6';
        // $query_args['post_type'] ='product';
        // $query_args['tax_query'] =  array(
        //     array(
        //         'taxonomy' => 'product_cat',
        //         'field' => 'slug',
        //         'terms' => $terms
        //     )
        // );
        // query_posts( $query_args );
        // print_r($products);
    }
}

add_action('pre_get_posts', 'custom_pre_get_posts_query');



/**
 * Display product attribute archive links

add_action( 'woocommerce_product_meta_end', 'wc_show_attribute_links' );
// if you'd like to show it on archive page, replace "woocommerce_product_meta_end" with "woocommerce_shop_loop_item_title"

function wc_show_attribute_links() {
    global $post;
    $attribute_names = array( '<pa_moments>', '<pa_weight>' ); // Add attribute names here and remember to add the pa_ prefix to the attribute name

    foreach ( $attribute_names as $attribute_name ) {
        $taxonomy = get_taxonomy( $attribute_name );

        if ( $taxonomy && ! is_wp_error( $taxonomy ) ) {
            $terms = wp_get_post_terms( $post->ID, $attribute_name );
            $terms_array = array();

            if ( ! empty( $terms ) ) {
                foreach ( $terms as $term ) {
                   $archive_link = get_term_link( $term->slug, $attribute_name );
                   $full_line = '<a href="' . $archive_link . '">'. $term->name . '</a>';
                   array_push( $terms_array, $full_line );
                }
                echo $taxonomy->labels->name . ' ' . implode( $terms_array, ', ' );
            }
        }
    }
}*/
/**
 * Remove product content based on category

add_action( 'wp', 'remove_product_content' );
function remove_product_content() {
    // If a product in the 'Cookware' category is being viewed...
    if ( is_product() && has_term( 'Cookware', 'product_cat' ) ) {
        //... Remove the images
        remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
        // For a full list of what can be removed please see woocommerce-hooks.php
    }
}*/
/*
 * Add customer email to Cancelled Order recipient list
 */
 function wc_cancelled_order_add_customer_email($recipient, $order)
 {
     return $recipient . ',' . $order->billing_email;
 }
 add_filter('woocommerce_email_recipient_cancelled_order', 'wc_cancelled_order_add_customer_email', 10, 2);


/**
 * @snippet       Disable Payment Method for Specific Category
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @author        Rodolfo Melogli
 * @compatible    WC 3.6.5
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
  
add_filter('woocommerce_available_payment_gateways', 'bbloomer_unset_gateway_by_category');
  
function bbloomer_unset_gateway_by_category($available_gateways)
{
    if (is_admin()) {
        return $available_gateways;
    }
    if (! is_checkout()) {
        return $available_gateways;
    }
    $unset = false;
    $category_ids = [ 70 ];
    foreach (WC()->cart->get_cart_contents() as $key => $values) {
        $terms = get_the_terms($values['product_id'], 'product_cat');
        foreach ($terms as $term) {
            if (in_array($term->term_id, $category_ids)) {
                $unset = true;
                break;
            }
        }
    }
    if ($unset == true) {
        unset($available_gateways['authorize_net_cim_credit_card']);
    }
    return $available_gateways;
}
